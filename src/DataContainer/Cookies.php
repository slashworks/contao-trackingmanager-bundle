<?php

namespace Slashworks\ContaoTrackingManagerBundle\DataContainer;

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\DataContainer;
use Slashworks\ContaoTrackingManagerBundle\Model\Cookie;
use Symfony\Component\VarDumper\VarDumper;

class Cookies
{

    public function onloadCallback(DataContainer $dc)
    {
        $cookie = Cookie::findByPk($dc->id);

        if(Null === $cookie){
            return;
        }

        // Add template selection to all cookies, except the base cookie.
        if (!$cookie->isBaseCookie) {
            PaletteManipulator::create()
                ->addLegend('template_legend', 'title_legend', PaletteManipulator::POSITION_AFTER)
                ->addField('templates', 'template_legend', PaletteManipulator::POSITION_APPEND)
                ->applyToPalette('default', Cookie::getTable());
        }
    }

    /**
     * @return array
     */
    public function getTemplates()
    {

        $arrAllTemplates = array();

        /** @var SplFileInfo[] $files */
        $files = \Contao\System::getContainer()->get('contao.resource_finder')->findIn('templates')->files()->name('/\.html5$/');

        foreach ($files as $file)
        {
            $strRelpath = \Contao\StringUtil::stripRootDir($file->getPathname());
            $strModule = preg_replace('@^(vendor/([^/]+/[^/]+)/|system/modules/([^/]+)/).*$@', '$2$3', strtr($strRelpath, '\\', '/'));
            $arrAllTemplates[$strModule][str_replace('.html5','',basename($strRelpath))] = basename($strRelpath);
        }

        $strError = '';

        // Copy an existing template
        if (\Contao\Input::post('FORM_SUBMIT') == 'tl_create_template')
        {
            $strOriginal = \Contao\Input::post('original', true);

            if (\Contao\Validator::isInsecurePath($strOriginal))
            {
                throw new RuntimeException('Invalid path ' . $strOriginal);
            }

            $strTarget = \Contao\Input::post('target', true);

            if (\Contao\Validator::isInsecurePath($strTarget))
            {
                throw new RuntimeException('Invalid path ' . $strTarget);
            }

            $projectDir = \Contao\System::getContainer()->getParameter('kernel.project_dir');

            // Validate the target path
            if (strncmp($strTarget, 'templates', 9) !== 0 || !is_dir($projectDir . '/' . $strTarget))
            {
                $strError = sprintf($GLOBALS['TL_LANG']['tl_templates']['invalid'], $strTarget);
            }
            else
            {
                $blnFound = false;
            }
        }

        return $arrAllTemplates;
    }

}
