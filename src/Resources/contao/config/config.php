<?php

/**
 * Register backend modules
 */
$GLOBALS['BE_MOD']['trackingmanager'] = array(

    'tmCookies' => array
    (
        'tables'      => array('tl_tm_cookie'),
        'createBase' => array('Slashworks\ContaoTrackingManagerBundle\Classes\CreateBase', 'createBase'),
    ),
    'tmStatistics' => array
    (
        'callback' => \Slashworks\ContaoTrackingManagerBundle\Classes\Backend\StatisticsDataManager::class,
        'tables'      => array('tl_tm_statistic'),
    )
);


/**
 * Register models
 */
$GLOBALS['TL_MODELS']['tl_tm_cookie'] = \Slashworks\ContaoTrackingManagerBundle\Model\Cookie::class;
$GLOBALS['TL_MODELS']['tl_tm_statistic'] = \Slashworks\ContaoTrackingManagerBundle\Model\Statistic::class;


/**
 * Register hooks
 */
$GLOBALS['TL_HOOKS']['generatePage'][] = array(\Slashworks\ContaoTrackingManagerBundle\Hook\GeneratePage::class, 'generateTrackingManager');
$GLOBALS['TL_HOOKS']['parseFrontendTemplate'][] = array(\Slashworks\ContaoTrackingManagerBundle\Hook\ParseFrontendTemplate::class, 'checkCookieDependency');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array(\Slashworks\ContaoTrackingManagerBundle\Hook\ReplaceInsertTags::class, 'replaceTrackingManagerEditor');


/**
 * Cron jobs
 */
$GLOBALS['TL_CRON']['daily'][] = array(\Slashworks\ContaoTrackingManagerBundle\Cron\UnknownCookieNotification::class, 'run');


/**
 * allowed pagetypes config
 */
$GLOBALS['TM_PAGETYPES'] = ['regular'];


/**
 * Asset handling
 */
if (TL_MODE === 'FE') {

} else {
    $combiner = new \Combiner();
    $combiner->add('/bundles/contaotrackingmanager/css/backend.scss');
    $GLOBALS['TL_CSS'][] = $combiner->getCombinedFile();
}
