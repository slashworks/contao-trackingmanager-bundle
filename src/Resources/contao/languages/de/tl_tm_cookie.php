<?php

// Buttons
$GLOBALS['TL_LANG']['tl_tm_cookie']['createBase'][0] = 'Basis-Konfiguration erstellen';
$GLOBALS['TL_LANG']['tl_tm_cookie']['createBase'][1] = 'Erstellt alle Cookies für die Basis-Konfiguration.';


// Legends
$GLOBALS['TL_LANG']['tl_tm_cookie']['title_legend'] = 'Allgemeine Einstellungen';
$GLOBALS['TL_LANG']['tl_tm_cookie']['template_legend'] = 'Template Einstellungen';


// Fields
$GLOBALS['TL_LANG']['tl_tm_cookie']['name'][0] = 'Name';
$GLOBALS['TL_LANG']['tl_tm_cookie']['name'][1] = 'Geben Sie den Namen des Cookies ein.';
$GLOBALS['TL_LANG']['tl_tm_cookie']['label'][0] = 'Label';
$GLOBALS['TL_LANG']['tl_tm_cookie']['label'][1] = 'Geben Sie das Label des Cookies ein.';
$GLOBALS['TL_LANG']['tl_tm_cookie']['isBaseCookie'][0] = 'Basis-Cookie';
$GLOBALS['TL_LANG']['tl_tm_cookie']['isBaseCookie'][1] = 'Dieser Cookie dient als Basis-Cookie zum Speichern der Einstellungen.';
$GLOBALS['TL_LANG']['tl_tm_cookie']['published'][0] = 'Veröffentlicht';
$GLOBALS['TL_LANG']['tl_tm_cookie']['published'][1] = 'Den Cookie veröffentlichen';
$GLOBALS['TL_LANG']['tl_tm_cookie']['disabled'][0] = 'Nur lesen';
$GLOBALS['TL_LANG']['tl_tm_cookie']['disabled'][1] = 'Cookieienstellung nicht bearbeitbar bei nur lesen';
$GLOBALS['TL_LANG']['tl_tm_cookie']['checked'][0] = 'Vorausgewählt';
$GLOBALS['TL_LANG']['tl_tm_cookie']['checked'][1] = 'Die Einstellung des Cookies auswählen';
$GLOBALS['TL_LANG']['tl_tm_cookie']['disabledHint'][0] = 'Hinweis bei nicht akzeptiertem Cookie';
$GLOBALS['TL_LANG']['tl_tm_cookie']['disabledHint'][1] = 'Wird anstelle des gewählten Templates ausgegeben wenn dieser Cookie nicht akzeptiert wurde. Mit dem Inserttag {{tm_editor::TEXT}} wird ein lin kgeneriert der den Manager erneut öffnet,';
$GLOBALS['TL_LANG']['tl_tm_cookie']['descriptions'][0] = 'Dienste-Cookies';
$GLOBALS['TL_LANG']['tl_tm_cookie']['descriptions'][1] = 'Die Liste aller Cookies des Dienstes';
$GLOBALS['TL_LANG']['tl_tm_cookie']['descriptionLabel'][0] = 'Name';
$GLOBALS['TL_LANG']['tl_tm_cookie']['descriptionLabel'][1] = 'Geben Sie den Namen des Dienste-Cookies ein.';
$GLOBALS['TL_LANG']['tl_tm_cookie']['description'][0] = 'Beschreibung';
$GLOBALS['TL_LANG']['tl_tm_cookie']['description'][1] = 'Geben Sie die Beschreibung des Dienste-Cookies ein.';
$GLOBALS['TL_LANG']['tl_tm_cookie']['templates'][0] = 'Templates';
$GLOBALS['TL_LANG']['tl_tm_cookie']['templates'][1] = 'Wählen Sie die Templates aus, deren Ausgabe nur bei akzeptierten Cookie-Einstellungen erfolgen soll.';

// Reference

$GLOBALS['TL_REF']['tl_tm_cookie']['checked']['auto'] ='Wenn Cookie gesetzt dann ausgewählt';
$GLOBALS['TL_REF']['tl_tm_cookie']['checked']['checked'] ='Immer ausgewählt';
