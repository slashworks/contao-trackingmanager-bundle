<?php

namespace Slashworks\ContaoTrackingManagerBundle\Hook;

use Contao\Input;
use Contao\LayoutModel;
use Contao\PageModel;
use Contao\PageRegular;
use Contao\System;
use Slashworks\ContaoTrackingManagerBundle\Classes\TrackingManager;
use Symfony\Component\VarDumper\VarDumper;

class GeneratePage
{
    /**
     *
     *
     * @param PageModel   $page
     * @param LayoutModel $layout
     * @param PageRegular $pageRegular
     */
    public function generateTrackingManager(PageModel $page, LayoutModel $layout, PageRegular $pageRegular)
    {
        $trackingManager = new TrackingManager();
        $trackingManager->setPage($page);
        $trackingManager->generate();
    }
}
